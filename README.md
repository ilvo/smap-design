!!! **IMPORTANT NOTICE** !!!

**The code of SMAP-Design is now a module of SMAP.
This project is no longer maintained, please refer to https://gitlab.ilvo.be/genomics/smap-package/smap**
